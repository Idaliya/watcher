
var fs = require('fs');
var url = require('url');
var request = require('request');
var express = require("express");
var cookieParser = require('cookie-parser');
var swig = require('swig');
var multiparty = require('multiparty');
var util = require('util');
var db;
var users;
var onlineUsers;
var User = require('./middleware.js');
var app = express();


app.use(cookieParser());

var bodyParser = require('body-parser')
app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));

app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/../views');
app.set('view cache', false);
swig.setDefaults({cache: false});

require('./mongo.js').init(function (err, _db) {
    if (err) throw err;
    db = _db;
    users = db.collection('test');
    onlineUsers = db.collection('onlineUsers');
    onlineGroups = db.collection('onlineGroups');
    periods = db.collection('periods');
    onlineFriends = db.collection('onlineFriends');
    periodsFriends = db.collection('periodsFriends');
    downloadTa = db.collection('downloadTa');
});

app.get('/', User.isLogged, function (req, res) {
    if (req.user) {
        res.redirect('/smm');
    } else {
        console.log('не смог найти пользователя')
        res.render('index');
    }
});

app.get('/smm', User.isLogged, User.getInfo, function (req, res) {

    if (!req.user) {
        return res.redirect('/');
    }
    if (req.user.groupsTA) {
        function getArrayOfPeriods(groupsTA, i, array) {
            if (i === groupsTA.length) {
                res.render('smm', {
                    photo: req.user.photo,
                    first_name: req.user.first_name,
                    last_name: req.user.last_name,
                    id: req.user.user_id,
                    groupsTA: req.user.groupsTA,
                    array: array
                });
                return;
            }
            var arrayOfIds = [];
            for (var j = 0; j < groupsTA[i].users.length; j++) {
                arrayOfIds[j] =  groupsTA[i].users[j].id;
            }
            periods.aggregate([{$project: {hour:1, commonToBoth: {$setIntersection: ['$ids', arrayOfIds ]} } }], function(err, result){
                if (err) {
                    return;
                }
                var groupPeriods = [];
                for (var k = 0; k < 24; k++) {
                    groupPeriods[k] = 0;
                }

                if (result) {
                    for (var j = result.length-1; j > result.length - 75;  j -- ) {
                        if (j < 0) break;
                        if (result[j].commonToBoth) {
                            if ((groupPeriods[result[j].hour] == 0) && (result[j].commonToBoth.length > 0)) {
                                groupPeriods[result[j].hour] = result[j].commonToBoth.length;
                            }

                        }
                    }
                    var obj = {
                        groupId: groupsTA[i].groupId,
                        periods: groupPeriods
                    };
                    array[i] = obj;
                    getArrayOfPeriods(groupsTA, i+1, array);
                } else {
                    getArrayOfPeriods(groupsTA, i+1, array);
                }

            });
        }
        getArrayOfPeriods(req.user.groupsTA, 0, []);
    }
    else if (req.user) {
        res.render('smm', {
            photo: req.user.photo,
            first_name: req.user.first_name,
            last_name: req.user.last_name,
            id: req.user.user_id});
    }

});

app.get('/exit', function (req, res) {
    res.clearCookie('id');
    res.redirect('/');
});

app.get('/profile', User.isLogged, User.getInfo, function (req, res) {
    if (req.user) {
        res.render('profile', {id: req.user.user_id, first_name: req.user.first_name, last_name: req.user.last_name, photo:req.user.photo});
        return;
    }
    res.redirect('/');

});

app.get('/vk', function (req, res) {
    var path = url.parse(req.url, true);
    if (!path.query.code) {
        res.redirect('/')
    }
    else {
        var tokenRequest = "https://oauth.vk.com/access_token?client_id=4755267&client_secret=Nlwa6Dl1WTUPiMJNv3e0&code=" + path.query.code + "&redirect_uri=37.139.44.122/vk";
        request(tokenRequest, function (error, response, body) {
            console.log('запрос выполнен');
            if (error) {
                res.send(500, "error");
                res.redirect('/');

            }
            else {
                var message = JSON.parse(body);
                console.log(message);

                res.cookie("id", message.access_token);

                users.findOne({user_id: message.user_id}, function (err, result) {

                    if (result) {
                        users.update({user_id: message.user_id}, {
                            $set: {
                                access_token: message.access_token,
                                secret: message.secret,
                                email: message.email
                            }
                        }, function (err, result) {
                            if (err) throw err;
                            console.log('я перезаписался');
                            res.redirect('/');
                        });

                    }
                    else {
                        users.insert(message, function (err, result) {
                            if (err) throw err;
                            console.log('я записался и получил токен');
                            res.redirect('/');
                        });
                    }
                });

            }

        });
    }
});

app.post('/addGroup',User.isLogged, User.getInfo, function (req, res) {
    console.log(req.body);
    console.log(req.body.city + '    my  usr in groups');

    var filter = {
        city:req.body.city,
        from: req.body.from,
        to: req.body.to,
        m: req.body.m,
        f: req.body.f
    };

    if (req.user) {
        if (req.body.idGroup && req.body.id) {
            User.getRealGroupId(req.body.idGroup,res, req.body.id, function(groupId, groupInfo, res) {
                User.getPostsIdsFromGroup(groupId, groupInfo, res, function(groupId, groupInfo, arr) {
                    User.getActiveUsers(groupId, groupInfo, arr,  function(mas, groupInfo) {
                        var i = mas.length, unickUsers = [];
                        mas.sort();
                        while(i--){
                            if(unickUsers.join('').search(mas[i]) == '-1') {
                                unickUsers.push(mas[i]);
                            }
                        }
                        console.log(unickUsers);
                        User.getUsersAfterFilters(filter, unickUsers, function(filters, array){
                            console.log(array.length + 'было');
                            var unickUsersAfterFilters = [];
                            var j = 0;
                            filters.from = parseInt(filters.from);
                            filters.to = parseInt(filters.to);

                            for (var i = 0; i < array.length; i++) {
                                if ((!filters.city) || ((array[i].city) && array[i].city.title == filters.city)){
                                    if ((!filters.m && !filters.f) || ((filters.m) && array[i].sex == 2) || ((filters.f) && array[i].sex == 1)) {
                                        if ((!filters.from && !filters.to) || (filters.from && !filters.to && array[i].bdate && array[i].bdate.length > 5 && parseInt(2015 - array[i].bdate.slice(array[i].bdate.length - 4, array[i].bdate.length)) > filters.from) || (!filters.from && filters.to && array[i].bdate && array[i].bdate && array[i].bdate.length > 5 && parseInt(2015 - array[i].bdate.slice(array[i].bdate.length - 4, array[i].bdate.length)) < filters.to) || (filters.from && filters.to && array[i].bdate && array[i].bdate.length > 5 && parseInt(2015 - array[i].bdate.slice(array[i].bdate.length - 4, array[i].bdate.length)) < filters.to && parseInt(2015 - array[i].bdate.slice(array[i].bdate.length - 4, array[i].bdate.length)) > filters.from)) {
                                            unickUsersAfterFilters[j] = array[i];
                                            j++
                                        }

                                    }
                                }
                            }
                            console.log(unickUsersAfterFilters.length + 'стало');

                            onlineGroups.findOne({'id':1} , function (err, result) {
                                if (err) throw err;
                                if (result) {
                                    onlineGroups.update(
                                        {'id': 1},
                                        { $push: { users: { $each: unickUsersAfterFilters } } }, function(err, result){

                                        });
                                } else {
                                    console.log('я тут просто тут но тут было пусто!!!!!!!!!!!!!');
                                    onlineGroups.insert({'id':1, 'users':unickUsersAfterFilters}, function (err, result) {
                                        if (err) throw err;
                                    });
                                }
                            });
                            console.log(unickUsersAfterFilters.length + 'количество активных пользователей');

                            var element = {
                                groupId: groupId,
                                groupName:groupInfo.name,
                                groupPhoto:groupInfo.photo_50,
                                usersNum:unickUsersAfterFilters.length,
                                users: unickUsersAfterFilters
                            };
                            users.update({user_id: req.user.user_id}, {
                                $push: {
                                    groupsTA:element
                                }
                            }, function (err, result) {
                                if (err) throw err;
                                res.redirect('/smm');

                            });
                        });
                    })
                });
            });

        } else console.log('моя группа не дошла!!!!!')
    }
    else {
        console.log(' юзер не зареган смм')
        return(res.redirect('/smm'));
    }

});

app.post('/addDownloadTa',User.isLogged, User.getInfo, function (req, res) {
    if (!req.user) {
    } else if (req.body) {
        console.log(req.body);
        var form = new multiparty.Form();
        form.parse(req, function(err, fields, files) {
            idGroup = fields.idGroup[0];
            id = fields.id[0];
            downloadUsers = fs.readFileSync(files.f[0].path,"utf8");
            var Users = downloadUsers.split(',');
            for (var i = 0; i < Users.length; i++){
                Users[i] = Number(Users[i]);
            }
            onlineGroups.findOne({'id':1} , function (err, result) {
                if (err) throw err;
                if (result) {
                    onlineGroups.update(
                        {'id': 1},
                        { $push: { users: { $each: Users} } }, function(err, result){
                        });
                } else {
                    console.log('я тут просто тут но тут было пусто!!!!!!!!!!!!!');
                    onlineGroups.insert({'id':1, 'users':Users}, function (err, result) {
                        if (err) throw err;
                    });
                }
            });

            var element = {
                groupId: idGroup,
                usersNum:Users.length,
                users: Users
            };
            users.update({user_id: req.user.user_id}, {
                $push: {
                    downloadTa:element
                }
            }, function (err, result) {
                if (err) throw err;
                res.redirect('/downloadTa');

            });
        });
    } else {
        console.log(' юзер не зареган смм')
        return(res.redirect('/smm'));
    }
});

app.get('/giveGroupTA', User.isLogged, function(req,res){
    var path = url.parse(req.url, true);
    console.log(path);
    if (!path.query.id) {
        res.redirect('/smm');
        return;
    }

    try {
        path.query.id = parseInt(path.query.id);
        path.query.groupId = parseInt(path.query.groupId);
    } catch(ignore) {}

    users.findOne({user_id : path.query.id}, function (err, result) {
        if (err) {
            return console.log('ошибока есть', err);
        }

        JSON.stringify(result);
        var groupTA = [];
        for (var i = 0; i < result.groupsTA.length; i++ ) {
            if (result.groupsTA[i].groupId === path.query.groupId) {
                console.log('он нашелся!')
                groupTA = result.groupsTA[i].users;
            }
        }
        if (!groupTA) {
            return;
        }
        var stream = '';

        for (i = 0; i < groupTA.length; i++ ) {
           stream = stream + groupTA[i].id + ',';
        }
        res.setHeader('Content-disposition', 'attachment; filename = TA.txt');
        res.setHeader('Content-type', 'text/plain');
        res.charset = 'UTF-8';
        res.write(stream);
        res.end();
    });

    });

app.get('/deleteGroup', User.isLogged, function(req,res) {
    var path = url.parse(req.url, true);

    if (!path.query.id) {
        res.redirect('/smm');
        return;
    }

    try {
        path.query.id = parseInt(path.query.id);
        path.query.groupId = parseInt(path.query.groupId);
        path.query.count = parseInt(path.query.count);
    } catch (ignore) {
    }
console.log(path.query.count + 'число удаления')
    users.update({user_id: path.query.id}, {'$pull': { groupsTA: {'groupId': path.query.groupId, 'usersNum':path.query.count}}}, function (err, result) {
        if (err) {
            return console.log('ошибока есть', err);
        }
        console.log(result + 'что то удалили');
        res.redirect('/smm');
    });
});

app.get('/downloadTa', User.isLogged, function(req,res) {
console.log('туточки')
    if (!req.user) {
        return res.redirect('/');
    }
    if (req.user.downloadTa) {
        function getArrayOfPeriods(downloadTa, i, array) {
            if (i === downloadTa.length) {
                res.render('downloadTa', {
                    photo: req.user.photo,
                    first_name: req.user.first_name,
                    last_name: req.user.last_name,
                    id: req.user.user_id,
                    groupsTA: req.user.downloadTa,
                    array: array
                });
                console.log(array);
                return;
            }
            var arrayOfIds = [];
            for (var j = 0; j < downloadTa[i].users.length; j++) {
                arrayOfIds[j] =  downloadTa[i].users[j];
            }

            periods.aggregate([{$project: {hour:1, commonToBoth: {$setIntersection: ['$ids', arrayOfIds ]} } }], function(err, result){
                if (err) {
                    return;
                }
                var groupPeriods = [];
                for (var k = 0; k < 24; k++) {
                    groupPeriods[k] = 0;
                }

                if (result) {
                    console.log(result);
                    for (var j = result.length-1; j > result.length - 75;  j -- ) {
                        if (j < 0) break;
                        if (result[j].commonToBoth) {
                            if ((groupPeriods[result[j].hour] == 0) && (result[j].commonToBoth.length > 0)) {
                                groupPeriods[result[j].hour] = result[j].commonToBoth.length;
                            }
                        }
                    }
                    var obj = {
                        groupId: downloadTa[i].groupId,
                        periods: groupPeriods
                    };
                    array[i] = obj;
                    getArrayOfPeriods(downloadTa, i+1, array);
                } else {
                    getArrayOfPeriods(downloadTa, i+1, array);
                }

            });
        }
        getArrayOfPeriods(req.user.downloadTa, 0, []);
    }
    else if (req.user) {
        res.render('downloadTa', {
            photo: req.user.photo,
            first_name: req.user.first_name,
            last_name: req.user.last_name,
            id: req.user.user_id});
    }
});

app.get('/deleteGroupDownload', User.isLogged, function(req,res) {
    var path = url.parse(req.url, true);

    if (!path.query.id) {
        res.redirect('/smm');
        return;
    }

    try {
        path.query.id = parseInt(path.query.id);
        path.query.count = parseInt(path.query.count);
        path.query.groupId = String(path.query.groupId);
    } catch (ignore) {
    }
    console.log(String(path.query.groupId) + 'то что пришло')
    users.update({user_id: path.query.id}, {'$pull': { downloadTa: {'groupId': path.query.groupId, 'usersNum':path.query.count}}}, function (err, result) {
        if (err) {
            return console.log('ошибока есть', err);
        }
        console.log(result + 'что то удалили');
        res.redirect('/downloadTa');
    });
});

app.listen(8090);