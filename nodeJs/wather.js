var request = require('request');
var bodyParser = require('body-parser')

require('./mongo.js').init(function (err, _db) {
    if (err) throw err;
    var db = _db;
    var onlineUsers = db.collection('onlineUsers');
    var periods = db.collection('periods');
    var onlineGroups = db.collection('onlineGroups');
    var onlineFriends = db.collection('onlineFriends');
    var periodsFriends = db.collection('periodsFriends');
    //var friendsPeriod = function () {
    //
    //    onlineFriends.find().toArray(function (err, result) {
    //        console.log('запрос');
    //        if (!result.length) {
    //            return;
    //        }
    //
    //        var url = "https://api.vk.com/method/users.get?user_ids="
    //        for (var i = 0; i < result.length - 1; i++) {
    //            url = url + result[i].id + ",";
    //
    //        }
    //        url = url + result[result.length - 1].id + "&v=5.29&fields=online";
    //        request(url, function (error, response, body) {
    //
    //            if (error) {
    //                return error;
    //            }
    //
    //            console.log('запрос выполнен');
    //            var message = JSON.parse(body);
    //            message = message.response;
    //
    //            var dateObj = new Date();
    //            console.log(dateObj);
    //
    //            console.log(message, 'this is res from vk');
    //            var ids = [];
    //            var j = 0;
    //            for (var i = 0; i < message.length; i++) {
    //                if (message[i].online && message[i].id) {
    //                    ids[j] = message[i].id;
    //                    j++;
    //                }
    //
    //            }
    //            console.log('id ' + ids);
    //
    //
    //            hour = dateObj.getHours();
    //            min = dateObj.getMinutes();
    //
    //            var doc = {
    //                ids: ids,
    //                hour:hour,
    //                min: min
    //            };
    //            periodsFriends.insert(doc, function(err, records){
    //                console.log("Record added as " + records[0]._id);
    //            });
    //
    //        });
    //
    //    });
    //}


    var callback = function(info){
        var dateObj = new Date();
        console.log(dateObj);
        hour = dateObj.getHours();
        min = dateObj.getMinutes();

        var doc = {
            ids: info,
            hour:hour
        }

        periods.insert(doc, function(err, records){
            console.log("Record added as "+ records[0]._id);
        });
    }

    var groupsPeriod = function() {
        var limit = 300;
        onlineGroups.find({'id':1}).toArray(function (err, result) {

            if (!result[0]) {
                console.log('no users');
                return;
            }
            if (result[0].users.length > 800) {
                var resultArray = [];
                var info = getPeriodGroupsMoreLimit(0, limit, result[0].users,  resultArray, limit, callback);
            }
            else {
                var info = getPeriodGroupsLessLimit(result[0].users, callback);
            }
        });
    }

    //setInterval(friendsPeriod, 1000*60*60);
    groupsPeriod();
    setInterval(groupsPeriod, 60*60*1000);
});
var getPeriodGroupsMoreLimit = function(from, to, array,  result, limit, callback){
    var url = "https://api.vk.com/method/users.get?user_ids="
    for (var i = from; i < to - 1; i++) {
        if (array[i].id) {
            url = url + array[i].id + ",";
        }
    }

    url = url + array[to - 1].id + "&v=5.29&fields=online";
    console.log(url);
    request(url, function (error, response, body) {
        if (error) {
            console.log(error);
            return;
        }
        var message = JSON.parse(response.body);
        message = message.response;
        var ids = [];
        var j = 0;
        for (var i = 0; i < message.length; i++) {
            if (message[i].online && message[i].id) {
                ids[j] = message[i].id;
                j++;
            }
        }
        result  = result.concat(ids);
        if (to == array.length) {

            return callback(result);
        }
        if (to + limit > array.length) {
            to = array.length;
        } else
            to = to + limit;
        from = from + limit;

        setTimeout(getPeriodGroupsMoreLimit(from, to, array, result, limit, callback), 1000);
    });
}
var getPeriodGroupsLessLimit = function(array, callback){

    var url = "https://api.vk.com/method/users.get?user_ids="
    for (var i = 0; i < array.length - 1; i++) {
        if (array[i].id) {
            url = url + array[i].id + ",";
        }

    }
    url = url + array[array.length - 1].id + "&v=5.29&fields=online";
    request(url, function (error, response, body) {

        if (error) {
            res.send(500, "error in request to vk");
            console.log(error);
            return res.redirect('/');
        }
        console.log(url);
        console.log('запрос выполнен');
        var message = JSON.parse(body);
        message = message.response;
        var ids = [];
        var j = 0;
        for (var i = 0; i < message.length; i++) {
            if (message[i].online && message[i].id) {
                ids[j] = message[i].id;
                j++;
            }

        }
        return callback(ids);
    });
};