var http = require('http');
var url = require('url');
var request = require('request');
var express = require("express");
var cookieParser = require('cookie-parser');
var md5 = require('MD5');
var db;
var users;
var onlineUsers;
var app = express();
app.use(cookieParser());

var bodyParser = require('body-parser')
app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));




require('./mongo.js').init(function (err, _db) {
    if (err) throw err;
    db = _db;
    users = db.collection('test');
    onlineUsers = db.collection('onlineUsers');
    onlineFriends = db.collection('onlineFriends');
});

module.exports.isLogged = function(req, res, next){
    if (req.cookies.id) {
        users.findOne({access_token: req.cookies.id}, function (err, result) {
            if (result) {
                req.user = result;
            }
            else {
                req.user = null;
            }
            next();
        })
    } else {
        req.user = null;
        next();
    }

};

module.exports.getInfo = function(req, res, next) {
    if (!req.user) {
        next();
        return;
    }
    if (req.user.photo) {
        next();
        return;
    }

    var requestToVk = 'https://api.vk.com/method/users.get?user_id=' + req.user.user_id + '&v=5.28&access_token=' + req.user.access_token+'&fields=photo_100,bdate,sex';
    var toMd5 = '/method/users.get?user_id=' + req.user.user_id + '&v=5.28&access_token=' + req.user.access_token+'&fields=photo_100,bdate,sex' + req.user.secret;
    var sig = md5(toMd5);
    requestToVk = requestToVk + '&sig=' + sig;
    request(requestToVk, function (error, response, body) {
        console.log('запрос выполнен на фото');
        if (error) {
            res.send(500, "error");
            res.redirect('/');
            return;
        }

        var message = JSON.parse(body);

        users.findOne({user_id: message.response[0].id}, function (err, result) {
            if (!result) {
                res.clearCookie('id');
                res.redirect('/');


            }

            users.update({user_id: message.response[0].id}, {
                $set: {
                    first_name: message.response[0].first_name,
                    last_name: message.response[0].last_name,
                    photo: message.response[0].photo_100,
                    bDate: message.response[0].bdate,
                    sex: message.response[0].sex
                }
            }, function (err, result) {
                if (err) throw err;
                console.log('я перезаписался c фото');
                res.redirect('/');


            });
        });
    });
}

module.exports.getUserId = function(str) {
    var id;
    var pos;

    pos = str.indexOf('id') + 1;

    if (pos && !isNaN(str.substring(pos + 1))) {
        return str.substring(pos + 1);
    }

    pos = str.indexOf('vk.com') + 1;
    if (pos) {
        id = str.substring(pos + 6);
        return id;
    }

    return str;

};

module.exports.getRealUserInfo = function(id, userId, res, callback){

    var requestToVk = 'https://api.vk.com/method/users.get?user_ids=' + id + '&v=5.28&fields=online';

    var info = request(requestToVk, function (error, response, body) {
        if (error) {
            res.send(500, "error");
            res.redirect('/');
            return;
        }

        var info = JSON.parse(body);
        console.log('инфа о юзере !!!!' + info.response);
        if (typeof(info.response) == 'undefined') {
            res.redirect('/');
            return;
        }
        callback(info, userId);
        return;
    });
};

module.exports.getStat = function(ids, callback) {
    var stat = [];
    for (var i = 0; i < ids.length; i++) {
        onlineFriends.findOne({id: ids[i]}, function (err, result) {
            if (!result) i++;
            else stat[i] = result.first_name;

        });
    }

    return stat;
};


module.exports.getRealGroupId = function(str, res, myUserId, callback) {
    var id;
    var pos;
    var groupId;
    pos = str.indexOf('club') + 1;
    if (pos && !isNaN(str.substring(pos + 1))) {
        console.log(str.substring(pos + 1) + '1');
        groupId = str.substring(pos + 1);

    } else {
        pos = str.indexOf('vk.com') + 1;

        if (pos) {
            id = str.substring(pos + 6);
            groupId = id;
        } else groupId = str;

    }
    console.log(groupId + 'группа!!!!');

    var requestToVk = 'https://api.vk.com/method/groups.getById?group_id=' + groupId + '&v=5.29';
    request(requestToVk, function (error, response, body) {
        if (error) {
            res.send(500, "error");
            console.log('ошибочка')
            res.redirect('/smm');
            return;
        }

        var info = JSON.parse(body);
        console.log(info);
        var groupInfo = {
            name:info.response[0].name,
            photo:info.response[0].photo_50
        }
        callback(info.response[0].id,groupInfo, res);
    });

};


module.exports.getPostsIdsFromGroup = function(id, groupInfo, res, callback){
    console.log('я тут!' + id)
    var requestToVk = 'https://api.vk.com/method/wall.get?owner_id=-' + id + '&v=5.28&count=10&filter=owner';
    request(requestToVk, function (error, response, body) {
        if (error) {
            res.send(500, "error");
            return;
        }

        var info = JSON.parse(body);


        if (typeof info.response == 'undefined') {
            console.log('info.response undefined ', info);
            res.redirect('/smm');
            return;
        } else {

            var arr = [];
            for (var i = 0; i < info.response.items.length; i++) {
                arr[i] = info.response.items[i].id
            }

           callback(id, arr, groupInfo);
        }
    });
};


module.exports.getActiveUsers = function(groupId, arr, groupInfo, callback) {

    var error = 0;
    var myRes = 0;
    var timeOut = 0;
    var mas = [];
    console.log(arr);
    var func = function(requestToVk) {
        timeOut = timeOut+1000;
        request(requestToVk, function (error, response, body) {
            if (error) {
                res.send(500, "error");
                error++;

            }

            var info = JSON.parse(body);

            mas = info.response.items.concat(mas);
            myRes++;
            if (myRes+error == arr.length) {
                return callback(mas, groupInfo);
            }

        })

    };


    for (var i = 0; i < arr.length; i++) {

        var requestToVk = 'https://api.vk.com/method/likes.getList?owner_id=-' + groupId + '&item_id=' + arr[i] + '&count=1000&type=post&v=5.29';

        setTimeout( func(requestToVk), timeOut);


        if (error) {
            console.log('was error!')
            return mas;
        }


    }



}

module.exports.getUsersAfterFilters = function(filter, array, next) {
    var limit = 300;
    if (array.length > limit) {
        getUserInfoMoreLimit(0, limit, array, [], limit,filter, next)
    } else {
        getUserInfoLessLimit(array, filter, next)
    }
}

var getUserInfoMoreLimit = function(from, to, array, result, limit, filter, next){
    var url = "https://api.vk.com/method/users.get?user_ids="

    for (var i = from; i < to - 1; i++) {
        url = url + array[i] + ",";
    }

    url = url + array[to - 1] + '&v=5.28&fields=sex,city,bdate';
    request(url, function (error, response, body) {

        if (error) {
            res.send(500, "error in request to vk");
            console.log(error);
            return res.redirect('/');
        }
        var message = JSON.parse(body);
        message = message.response;
        var ids = [];

        result  = result.concat(message);
        if (to == array.length) {
            console.log('всех юзеров проверили')
            return next(filter, result);
        }
        if (to + limit > array.length) {
            to = array.length;
        } else
            to = to + limit;
        from = from + limit;

        setTimeout(getUserInfoMoreLimit(from, to, array,  result, limit, filter, next), 1000);
    });
}
var getUserInfoLessLimit = function(array, filters, next){

    var url = "https://api.vk.com/method/users.get?user_ids="
    for (var i = 0; i < array.length - 1; i++) {
        url = url + array[i] + ",";
    }
    url = url + array[array.length - 1] + '&v=5.28&fields=sex,city,bdate';
    request(url, function (error, response, body) {

        if (error) {
            res.send(500, "error in request to vk");
            console.log(error);
            return res.redirect('/');
        }

        console.log('запрос выполнен');
        var message = JSON.parse(body);
        message = message.response;
        console.log(message);
        return next(filters, message);

    });
};



