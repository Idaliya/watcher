'use strict';
var Promise = require('es6-promise').Promise;

var NODE_VERSION = 'iojs-v2.2.1';

module.exports = function (shipit) {
    require('shipit-deploy')(shipit);

    shipit.initConfig({
        'default': {
            workspace: './worksapce',
            deployTo: '~/watcher',
            repositoryUrl: 'git@bitbucket.org:Idaliya/watcher.git',
            ignores: ['.git'],
            rsync: ['--del'],
            keepReleases: 2,
            shallowClone: false
        },
        staging: {
            servers: 'numerd@192.168.0.20'
        }
    });

    function withNode(command) {
        return ['. ~/.nvm_init', 'nvm use ' + NODE_VERSION, command].join(' && ');
    }

    function inReleasePath(command) {
        return ['cd ' + shipit.releasePath, command].join(' && ');
    }

    function foreverStart(uid, path) {
        return [
            'forever start',
            '--uid ' + uid,
            '--append',
            path
        ].join(' ');
    }

    function whichProcessForeverCantFind(msg) {
        var matched = msg.match(/Forever cannot find process with id: (.*)/);
        return !matched ? null : matched[1];
    }

    function handleForeverProcessNotFound(err) {
        var crashedAppUid = whichProcessForeverCantFind(err.message);
        if (!crashedAppUid) {
            throw err;
        }
        console.log('App', crashedAppUid, 'has not been started, skip stopping');
    }

    function stop(name) {
        return shipit.remote(withNode(inReleasePath('forever stop ' + name)));
    }

    function start(name, path) {
        return shipit.remote(withNode(inReleasePath(foreverStart(name, path))));
    }

    function restart(name, path) {
        return stop(name)
            .catch(handleForeverProcessNotFound)
            .then(start(name, path));
    }

    function restartAll() {
        return Promise.all([
            restart('watcher-app-server', 'nodeJs/server.js'),
            restart('watcher-app-watcher', 'nodeJs/wather.js')
        ]);
    }

    shipit.task('restart', function () {
        return restartAll();
    });

    shipit.blTask('npm-build', function () {
        return shipit.remote(withNode(inReleasePath('npm run-script build')));
    });

    shipit.task('list', function () {
        return shipit.remote(withNode('forever list'));
    });

    shipit.task('npm-install-g', function () {
        return shipit.remote(withNode('npm install forever -g'));
    });

    shipit.on('updated', function () {
        shipit.start('npm-build');
    });

    shipit.on('published', function () {
        shipit.start('restart');
    });

};