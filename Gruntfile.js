module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.initConfig(config);

    grunt.registerTask('default', ['less:development', 'watch']);
};


var config = {
    less: {
        development: {
            options: {},
            files: {
                "static/dist/css/main.css": 'static/less/bootstrap.less'
            }
        },
        production: {
            options: {
                compress: true,
                optimization: 3
            },
            files: {
                "static/dist/css/main.css": 'static/less/bootstrap.less'
            }
        }
    },
    watch: {
        watchCss: {
            files: 'static/less/**/*.less',
            tasks: 'less:development'
        }
    }
};